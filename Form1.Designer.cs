﻿namespace TomogrammVisualizer
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.glControl1 = new OpenTK.GLControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.gbRButtons = new System.Windows.Forms.GroupBox();
            this.rbTexture = new System.Windows.Forms.RadioButton();
            this.rbQuads = new System.Windows.Forms.RadioButton();
            this.tbMin = new System.Windows.Forms.TrackBar();
            this.tbMax = new System.Windows.Forms.TrackBar();
            this.lbMin = new System.Windows.Forms.Label();
            this.lbMax = new System.Windows.Forms.Label();
            this.gb = new System.Windows.Forms.GroupBox();
            this.gb2 = new System.Windows.Forms.GroupBox();
            this.gbSideView = new System.Windows.Forms.GroupBox();
            this.rbLeftSideView = new System.Windows.Forms.RadioButton();
            this.rbRightSideView = new System.Windows.Forms.RadioButton();
            this.rbAboveView = new System.Windows.Forms.RadioButton();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.gbRButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMax)).BeginInit();
            this.gb.SuspendLayout();
            this.gb2.SuspendLayout();
            this.gbSideView.SuspendLayout();
            this.SuspendLayout();
            // 
            // glControl1
            // 
            this.glControl1.BackColor = System.Drawing.Color.Black;
            this.glControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl1.Location = new System.Drawing.Point(3, 16);
            this.glControl1.Name = "glControl1";
            this.glControl1.Size = new System.Drawing.Size(717, 366);
            this.glControl1.TabIndex = 0;
            this.glControl1.VSync = false;
            this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(74, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.fileToolStripMenuItem.Text = "OpenFile";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(80, 0);
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(545, 45);
            this.trackBar1.TabIndex = 5;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // gbRButtons
            // 
            this.gbRButtons.Controls.Add(this.rbTexture);
            this.gbRButtons.Controls.Add(this.rbQuads);
            this.gbRButtons.Location = new System.Drawing.Point(621, 12);
            this.gbRButtons.Name = "gbRButtons";
            this.gbRButtons.Size = new System.Drawing.Size(103, 45);
            this.gbRButtons.TabIndex = 3;
            this.gbRButtons.TabStop = false;
            // 
            // rbTexture
            // 
            this.rbTexture.AutoSize = true;
            this.rbTexture.Location = new System.Drawing.Point(10, 27);
            this.rbTexture.Name = "rbTexture";
            this.rbTexture.Size = new System.Drawing.Size(61, 17);
            this.rbTexture.TabIndex = 1;
            this.rbTexture.Text = "Texture";
            this.rbTexture.UseVisualStyleBackColor = true;
            this.rbTexture.CheckedChanged += new System.EventHandler(this.rbTexture_CheckedChanged);
            // 
            // rbQuads
            // 
            this.rbQuads.AutoSize = true;
            this.rbQuads.Checked = true;
            this.rbQuads.Location = new System.Drawing.Point(10, 7);
            this.rbQuads.Name = "rbQuads";
            this.rbQuads.Size = new System.Drawing.Size(56, 17);
            this.rbQuads.TabIndex = 0;
            this.rbQuads.TabStop = true;
            this.rbQuads.Text = "Quads";
            this.rbQuads.UseVisualStyleBackColor = true;
            this.rbQuads.CheckedChanged += new System.EventHandler(this.rbQuads_CheckedChanged);
            // 
            // tbMin
            // 
            this.tbMin.Location = new System.Drawing.Point(80, 19);
            this.tbMin.Maximum = 300;
            this.tbMin.Name = "tbMin";
            this.tbMin.Size = new System.Drawing.Size(545, 45);
            this.tbMin.TabIndex = 6;
            this.tbMin.Scroll += new System.EventHandler(this.tbMin_Scroll);
            // 
            // tbMax
            // 
            this.tbMax.Location = new System.Drawing.Point(80, 41);
            this.tbMax.Maximum = 3000;
            this.tbMax.Minimum = 300;
            this.tbMax.Name = "tbMax";
            this.tbMax.Size = new System.Drawing.Size(545, 45);
            this.tbMax.TabIndex = 5;
            this.tbMax.Value = 300;
            this.tbMax.Scroll += new System.EventHandler(this.tbMax_Scroll);
            // 
            // lbMin
            // 
            this.lbMin.AutoSize = true;
            this.lbMin.Location = new System.Drawing.Point(50, 24);
            this.lbMin.Name = "lbMin";
            this.lbMin.Size = new System.Drawing.Size(24, 13);
            this.lbMin.TabIndex = 6;
            this.lbMin.Text = "Min";
            // 
            // lbMax
            // 
            this.lbMax.AutoSize = true;
            this.lbMax.Location = new System.Drawing.Point(50, 51);
            this.lbMax.Name = "lbMax";
            this.lbMax.Size = new System.Drawing.Size(27, 13);
            this.lbMax.TabIndex = 7;
            this.lbMax.Text = "Max";
            // 
            // gb
            // 
            this.gb.Controls.Add(this.gbSideView);
            this.gb.Controls.Add(this.tbMax);
            this.gb.Controls.Add(this.lbMin);
            this.gb.Controls.Add(this.lbMax);
            this.gb.Controls.Add(this.tbMin);
            this.gb.Controls.Add(this.menuStrip1);
            this.gb.Controls.Add(this.gbRButtons);
            this.gb.Controls.Add(this.trackBar1);
            this.gb.Dock = System.Windows.Forms.DockStyle.Top;
            this.gb.Location = new System.Drawing.Point(0, 0);
            this.gb.Name = "gb";
            this.gb.Size = new System.Drawing.Size(723, 125);
            this.gb.TabIndex = 8;
            this.gb.TabStop = false;
            // 
            // gb2
            // 
            this.gb2.Controls.Add(this.glControl1);
            this.gb2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gb2.Location = new System.Drawing.Point(0, 125);
            this.gb2.Name = "gb2";
            this.gb2.Size = new System.Drawing.Size(723, 385);
            this.gb2.TabIndex = 9;
            this.gb2.TabStop = false;
            // 
            // gbSideView
            // 
            this.gbSideView.Controls.Add(this.rbAboveView);
            this.gbSideView.Controls.Add(this.rbRightSideView);
            this.gbSideView.Controls.Add(this.rbLeftSideView);
            this.gbSideView.Location = new System.Drawing.Point(621, 62);
            this.gbSideView.Name = "gbSideView";
            this.gbSideView.Size = new System.Drawing.Size(96, 73);
            this.gbSideView.TabIndex = 8;
            this.gbSideView.TabStop = false;
            // 
            // rbLeftSideView
            // 
            this.rbLeftSideView.AutoSize = true;
            this.rbLeftSideView.Location = new System.Drawing.Point(6, 0);
            this.rbLeftSideView.Name = "rbLeftSideView";
            this.rbLeftSideView.Size = new System.Drawing.Size(87, 17);
            this.rbLeftSideView.TabIndex = 0;
            this.rbLeftSideView.TabStop = true;
            this.rbLeftSideView.Text = "LeftSideView";
            this.rbLeftSideView.UseVisualStyleBackColor = true;
            this.rbLeftSideView.CheckedChanged += new System.EventHandler(this.rbLeftSideView_CheckedChanged);
            // 
            // rbRightSideView
            // 
            this.rbRightSideView.AutoSize = true;
            this.rbRightSideView.Location = new System.Drawing.Point(6, 23);
            this.rbRightSideView.Name = "rbRightSideView";
            this.rbRightSideView.Size = new System.Drawing.Size(94, 17);
            this.rbRightSideView.TabIndex = 1;
            this.rbRightSideView.TabStop = true;
            this.rbRightSideView.Text = "RightSideView";
            this.rbRightSideView.UseVisualStyleBackColor = true;
            this.rbRightSideView.CheckedChanged += new System.EventHandler(this.rbRightSideView_CheckedChanged);
            // 
            // rbAboveView
            // 
            this.rbAboveView.AutoSize = true;
            this.rbAboveView.Location = new System.Drawing.Point(6, 46);
            this.rbAboveView.Name = "rbAboveView";
            this.rbAboveView.Size = new System.Drawing.Size(79, 17);
            this.rbAboveView.TabIndex = 2;
            this.rbAboveView.TabStop = true;
            this.rbAboveView.Text = "AboveView";
            this.rbAboveView.UseVisualStyleBackColor = true;
            this.rbAboveView.CheckedChanged += new System.EventHandler(this.rbAboveView_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 510);
            this.Controls.Add(this.gb2);
            this.Controls.Add(this.gb);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.gbRButtons.ResumeLayout(false);
            this.gbRButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbMax)).EndInit();
            this.gb.ResumeLayout(false);
            this.gb.PerformLayout();
            this.gb2.ResumeLayout(false);
            this.gbSideView.ResumeLayout(false);
            this.gbSideView.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.GroupBox gbRButtons;
        private System.Windows.Forms.RadioButton rbTexture;
        private System.Windows.Forms.RadioButton rbQuads;
        private System.Windows.Forms.TrackBar tbMin;
        private System.Windows.Forms.TrackBar tbMax;
        private System.Windows.Forms.Label lbMin;
        private System.Windows.Forms.Label lbMax;
        private System.Windows.Forms.GroupBox gb;
        private System.Windows.Forms.GroupBox gb2;
        private System.Windows.Forms.GroupBox gbSideView;
        private System.Windows.Forms.RadioButton rbRightSideView;
        private System.Windows.Forms.RadioButton rbLeftSideView;
        private System.Windows.Forms.RadioButton rbAboveView;
    }
}

