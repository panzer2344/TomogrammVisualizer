﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace TomogrammVisualizer
{
    public partial class Form1 : Form
    {
        Bin bin;
        bool loaded;
        View view;
        int currentLayer;
        int FrameCount;
        DateTime NextFPSUpdate = DateTime.Now.AddSeconds(1);
        bool needReload;


        public Form1()
        {
            loaded = false;
            currentLayer = 1;
            bin = new Bin();
            view = new View();
            needReload = false;

            InitializeComponent();
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK) {
                string str = dialog.FileName;
                bin.readBIN(str);

                if (rbLeftSideView.Checked)
                {
                    trackBar1.Maximum = Bin.X - 1;
                    view.SetupSideView(glControl1.Width, glControl1.Height);

                }
                else
                {
                    if (rbRightSideView.Checked)
                    {
                        trackBar1.Maximum = Bin.Y - 1;
                        view.SetupRightSideView(glControl1.Width, glControl1.Height);
                    }
                    else
                    {
                        trackBar1.Maximum = Bin.Z - 1;
                        view.SetupView(glControl1.Width, glControl1.Height);
                    }
                }

                loaded = true;
                glControl1.Invalidate();
            }
        }
        
        private void glControl1_Paint(object sender, PaintEventArgs e)
        {
            if (loaded) {
                if (rbQuads.Checked){
                    //view.DrawQuads(currentLayer);
                    if (rbLeftSideView.Checked){
                        view.DrawQuadStripSideView(currentLayer);
                    }
                    else{
                        if (rbRightSideView.Checked)
                        {
                            view.DrawQuadStripRightSideView(currentLayer);
                        }
                        else
                        {
                            view.DrawQuadStrip(currentLayer);
                        }
                    }
                }

                if (rbTexture.Checked){
                    if (needReload){
                        if (rbLeftSideView.Checked){
                            view.generateSideViewTextureImage(currentLayer);
                        }
                        else{
                            if (rbRightSideView.Checked)
                            {
                                view.generateRightSideViewTextureImage(currentLayer);
                            }
                            else
                            {
                                view.generateTextureImage(currentLayer);
                            }
                        }
                        view.Load2DTexture();
                        needReload = false;
                    }

                    int width = 0, height = 0;
                    if (rbLeftSideView.Checked)
                    {
                        width = Bin.Y;
                        height = Bin.Z;
                    }
                    else {
                        if (rbRightSideView.Checked)
                        {
                            width = Bin.X;
                            height = Bin.Z;
                        }
                        else
                        {
                            width = Bin.X;
                            height = Bin.Y;
                        }
                    }
                    view.DrawTexture(width, height);
                }

                glControl1.SwapBuffers(); 
            }
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            currentLayer = trackBar1.Value;
            if (rbTexture.Checked) needReload = true; 
        }

        void Application_Idle(object sender, EventArgs e) {
            while (glControl1.IsIdle) {
                displayFPS();
                glControl1.Invalidate();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Application.Idle += Application_Idle;
        }

        void displayFPS() {
            if (DateTime.Now >= NextFPSUpdate) {
                this.Text = String.Format("CT Visualizer (fps={0})", 
                                            FrameCount);
                NextFPSUpdate = DateTime.Now.AddSeconds(1);
                FrameCount = 0;
            }
            FrameCount++;
        }

        private void tbMin_Scroll(object sender, EventArgs e)
        {
            view.SetMin(tbMin.Value);
            if (rbTexture.Checked) needReload = true; 
        }

        private void tbMax_Scroll(object sender, EventArgs e)
        {
            view.SetMax(tbMax.Value);
            if (rbTexture.Checked) needReload = true; 
        }

        private void rbTexture_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTexture.Checked) needReload = true;
        }

        private void rbQuads_CheckedChanged(object sender, EventArgs e)
        {
            //if (rbTexture.Checked) needReload = true;
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            glControl1.Invalidate();
            if (rbTexture.Checked) needReload = true;
        }

        private void rbLeftSideView_CheckedChanged(object sender, EventArgs e)
        {
            ChangeView("LeftSide");
        }

        private void rbRightSideView_CheckedChanged(object sender, EventArgs e)
        {
            ChangeView("RightSide");
        }

        private void rbAboveView_CheckedChanged(object sender, EventArgs e)
        {
            ChangeView("AboveView");
        }

        private void ChangeView(string s) {
            switch (s) {
                case "LeftSide":
                    {
                        trackBar1.Maximum = Bin.X - 1;
                        currentLayer = 0;
                        glControl1.Invalidate();
                        view.SetupSideView(glControl1.Width, glControl1.Height);

                        break;
                    }

                case "AboveView":
                    {
                        currentLayer = 0;
                        glControl1.Invalidate();
                        trackBar1.Maximum = Bin.Z - 1;
                        view.SetupView(glControl1.Width, glControl1.Height);

                        break;
                    }

                case "RightSide":
                    {
                        trackBar1.Maximum = Bin.Y - 1;
                        currentLayer = 0;
                        glControl1.Invalidate();
                        view.SetupRightSideView(glControl1.Width, glControl1.Height);

                        break;
                    }
            }
        }
    }
}
