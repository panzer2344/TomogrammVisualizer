﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using System.Drawing.Imaging;

namespace TomogrammVisualizer
{
    class View
    {
        private int min = 0;
        private int max = 2000;

        public void SetMin(int value) {
            min = value;
        }

        public int GetMin(int value) {
            return min;
        }

        public void SetMax(int value)
        {
            max = value;
        }

        public int GetMax(int value)
        {
            return max;
        }

        public void SetupView(int width, int height) {
            int newWidth = (int)(height * Bin.coefAbove);
            int offsetX = (width - newWidth) / 2;

            GL.ShadeModel(ShadingModel.Smooth);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, Bin.X, 0, Bin.Y, -1, 1);
            GL.Viewport(offsetX, 0, newWidth, height);
        }

        public void SetupSideView(int width, int height)
        {
            int newWidth = width;//(int)(height * Bin.coefSide);
            int newHeight = height;//(int)(width * Bin.coefSide);
            int offsetX = 0;// (width - newWidth) / 2;
            int offsetY = 0;// height / 3;//height / 3;

            GL.ShadeModel(ShadingModel.Smooth);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, Bin.Y, 0, Bin.Z, -1, 1);
            GL.Viewport(offsetX, offsetY, newWidth, newHeight);
        }

        public void SetupRightSideView(int width, int height)
        {
            int newWidth = width;//(int)(height * Bin.coefSide);
            int newHeight = height;//(int)(width * Bin.coefSide);
            int offsetX = 0;// (width - newWidth) / 2;
            int offsetY = 0;// height / 3;//height / 3;

            GL.ShadeModel(ShadingModel.Smooth);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, Bin.X, 0, Bin.Z, -1, 1);
            GL.Viewport(offsetX, offsetY, newWidth, newHeight);
        }

        public Color TransferFunction(short value)
        {
            int newVal = clamp((value - min) * 255 / (max - min), 0, 255);

            return Color.FromArgb(255, newVal, newVal, newVal);
        }

        protected int clamp(float value, int min, int max) {
            if (value > min) return max;
            if (value < min) return min;
            return (int)value; 
        }

        public void DrawQuads(int layerNumber) {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Begin(BeginMode.Quads);
            for (int x_coord = 0; x_coord < Bin.X - 1; x_coord++) {
                for (int y_coord = 0; y_coord < Bin.Y - 1; y_coord++) {
                    short value;

                    value = Bin.array[x_coord + y_coord * Bin.X
                                        + layerNumber * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(x_coord, y_coord);

                    value = Bin.array[x_coord + (y_coord + 1) * Bin.X
                                        + layerNumber * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(x_coord, y_coord + 1);

                    value = Bin.array[x_coord + 1 + (y_coord + 1) * Bin.X
                                        + layerNumber * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(x_coord + 1, y_coord + 1);

                    value = Bin.array[x_coord + 1 + y_coord  * Bin.X
                                       + layerNumber * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(x_coord + 1, y_coord);
                }
            }
            GL.End();
        }

        public void DrawQuadStrip(int layerNumber) {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            for (int y_coord = 0; y_coord < Bin.Y - 1; y_coord++)
            {
                GL.Begin(BeginMode.QuadStrip);

                short value;

                value = Bin.array[y_coord * Bin.X + layerNumber * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(0, y_coord);

                value = Bin.array[(y_coord + 1) * Bin.X + layerNumber * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(0, y_coord + 1);

                value = Bin.array[1 + y_coord * Bin.X + layerNumber * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(1, y_coord);

                value = Bin.array[1 + (y_coord + 1) * Bin.X + layerNumber * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(1, y_coord + 1);

                for (int x_coord = 2; x_coord < Bin.X - 1; x_coord++)
                {
                    value = Bin.array[x_coord + y_coord * Bin.X
                                        + layerNumber * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(x_coord, y_coord);

                    value = Bin.array[x_coord + (y_coord + 1) * Bin.X
                                        + layerNumber * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(x_coord, y_coord + 1);
                }

                GL.End();
            }
        }

        public void DrawQuadStripSideView(int layerNumber)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            for (int z_coord = 0; z_coord < Bin.Z - 1; z_coord++)
            {
                GL.Begin(BeginMode.QuadStrip);

                short value;

                value = Bin.array[layerNumber + z_coord * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(0, z_coord);

                value = Bin.array[layerNumber + (z_coord + 1) * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(0, z_coord + 1);

                value = Bin.array[layerNumber + Bin.X + z_coord * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(1, z_coord);

                value = Bin.array[Bin.X + layerNumber + (z_coord + 1) * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(1, z_coord + 1);

                for (int y_coord = 2; y_coord < Bin.Y - 1; y_coord++)
                {
                    value = Bin.array[layerNumber + y_coord * Bin.X + z_coord * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(y_coord, z_coord);

                    value = Bin.array[layerNumber + y_coord * Bin.X + (z_coord + 1) * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(y_coord, z_coord + 1);
                }

                GL.End();
            }
        }

        public void DrawQuadStripRightSideView(int layerNumber)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            for (int z_coord = 0; z_coord < Bin.Z - 1; z_coord++)
            {
                GL.Begin(BeginMode.QuadStrip);

                short value;

                value = Bin.array[layerNumber * Bin.X + z_coord * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(0, z_coord);

                value = Bin.array[layerNumber * Bin.X + (z_coord + 1) * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(0, z_coord + 1);

                value = Bin.array[1 + layerNumber * Bin.X + z_coord * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(1, z_coord);

                value = Bin.array[1 + layerNumber * Bin.X + (z_coord + 1) * Bin.X * Bin.Y];
                GL.Color3(TransferFunction(value));
                GL.Vertex2(1, z_coord + 1);

                for (int x_coord = 2; x_coord < Bin.X - 1; x_coord++)
                {
                    value = Bin.array[x_coord + layerNumber * Bin.X + z_coord * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(x_coord, z_coord);

                    value = Bin.array[x_coord + layerNumber * Bin.X + (z_coord + 1) * Bin.X * Bin.Y];
                    GL.Color3(TransferFunction(value));
                    GL.Vertex2(x_coord, z_coord + 1);
                }

                GL.End();
            }
        }

        Bitmap textureImage;
        int VBOtexture;

        public void Load2DTexture() {
            GL.BindTexture(TextureTarget.Texture2D, VBOtexture);
            BitmapData data = textureImage.LockBits(
                    new System.Drawing.Rectangle(0, 0, textureImage.Width, textureImage.Height),
                    ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppArgb
                    );

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba,
                data.Width, data.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
                PixelType.UnsignedByte, data.Scan0
                );

            textureImage.UnlockBits(data);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter,
                (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter,
                (int)TextureMagFilter.Linear);

            ErrorCode Er = GL.GetError();
            string str = Er.ToString();
        }

        public void generateTextureImage(int layerNumber) {
            textureImage = new Bitmap(Bin.X, Bin.Y);
            for (int i = 0; i < Bin.X; ++i) {
                for (int j = 0; j < Bin.Y; ++j) {
                    int pixelNumber = i + j * Bin.X + layerNumber * Bin.X * Bin.Y;
                    textureImage.SetPixel(i, j, TransferFunction(Bin.array[pixelNumber]));
                }
            }
        }

        public void generateSideViewTextureImage(int layerNumber){
            textureImage = new Bitmap(Bin.Y, Bin.Z);
            for (int i = 0; i < Bin.Y; ++i){
                for (int j = 0; j < Bin.Z; ++j){
                    int pixelNumber = layerNumber + i * Bin.X + j * Bin.X * Bin.Y;
                    textureImage.SetPixel(i, j, TransferFunction(Bin.array[pixelNumber]));
                }
            }
        }

        public void generateRightSideViewTextureImage(int layerNumber)
        {
            textureImage = new Bitmap(Bin.X, Bin.Z);
            for (int i = 0; i < Bin.X; ++i)
            {
                for (int j = 0; j < Bin.Z; ++j)
                {
                    int pixelNumber = i + layerNumber * Bin.X + j * Bin.X * Bin.Y;
                    textureImage.SetPixel(i, j, TransferFunction(Bin.array[pixelNumber]));
                }
            }
        }

        public void DrawTexture(int width, int height) {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.Enable(EnableCap.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, VBOtexture);

            GL.Begin(BeginMode.Quads);
            GL.Color3(Color.White);
            GL.TexCoord2(0f, 0f);
            GL.Vertex2(0, 0);
            GL.TexCoord2(0f, 1f);
            GL.Vertex2(0, height);
            GL.TexCoord2(1f, 1f);
            GL.Vertex2(width, height);
            GL.TexCoord2(1f, 0f);
            GL.Vertex2(width, 0);
            GL.End();

            GL.Disable(EnableCap.Texture2D);

        }

    }
}
